#!/usr/bin/env python3.7
import argparse
import json
import logging
import os
import sys
import requests


logging.basicConfig(format="%(asctime)-15s [%(levelname)s]: %(message)s")
logger = logging.getLogger(__name__)

USER = os.getenv("USER")


def _get_cognito_access_token(args):
    if args.deployment == "prd":
        pool_name = "kubioscloud"
    elif args.deployment == "stg":
        pool_name = "kubioscloud-stg"
    elif args.deployment == "dev" and args.user:
        pool_name = "kubioscloud-{}".format(args.user)
    else:
        raise ValueError

    url = "https://{}.auth.{}.amazoncognito.com/oauth2/token".format(
        pool_name, args.aws_region
    )

    data = {"client_id": args.client_id, "grant_type": "client_credentials"}
    if args.scopes:
        data["scope"] = " ".join(args.scopes.split(","))

    logger.debug(
        "Authenticating to '{}' with client_id: {}".format(url, args.client_id)
    )
    response = requests.post(
        url=url, data=data, auth=(args.client_id, args.client_secret)
    )

    if response:
        logger.debug("Authentication successful.")
        return response.json()["access_token"], None
    else:
        return None, response.json()["error"]


def _api_call(args, reqs, verb, path, params=None, data=None):
    if args.deployment == "dev":
        api_base_url = "https://analysis.{}.dev.kubioscloud.com".format(args.user)
    elif args.deployment == "stg":
        api_base_url = "https://analysis.stg.kubioscloud.com"
    elif args.deployment == "prd":
        api_base_url = "https://analysis.kubioscloud.com"
    else:
        raise ValueError

    response = reqs.request(verb, url=api_base_url + path, params=params, json=data)

    if response:
        return response.json(), None
    else:
        output_json = response.json()
        return None, output_json.get("error", output_json)


def do_analytics_readiness(args, reqs):
    if len(args.OPERATION) < 3:
        return (
            None,
            "Too few parameters for analytics_readiness: {}".format(
                len(args.OPERATION) - 1
            ),
        )

    data_format = args.OPERATION[1]
    if data_format not in ("RR", "PPI"):
        return None, "Invalid format for analyze readiness: {}".format(data_format)

    total_values = 0
    data = []
    for values_array in args.OPERATION[2:]:
        values = [int(value) for value in values_array.split(",")]
        # data.append({"values": values})
        data += values
        total_values += len(values)

    data = {
        "type": "RRI",
        "data": data,
        "analysis": {"type": "readiness", "history": []},
    }

    logger.debug(data)
    verb = "POST"
    path = "/v2/analytics/analyze"
    logger.debug(
        "Performing API CALL '{} {}' with format '{}' and {} values".format(
            verb, path, data_format, total_values
        )
    )
    return _api_call(args, reqs, verb, path, data=data)


def do_analytics_training(args, reqs):
    if len(args.OPERATION) < 2:
        return (
            None,
            "Too few parameters for analytics_training: {}".format(
                len(args.OPERATION) - 1
            ),
        )

    with open(args.OPERATION[1], mode="r", encoding="utf8") as fh:
        data = json.load(fh)

    logger.debug(data)
    verb = "POST"
    path = "/v2/analytics/analyze"
    logger.debug("Performing API CALL '{} {}'".format(verb, path))
    return _api_call(args, reqs, verb, path, data=data)


def main():
    parser = argparse.ArgumentParser(description="KubiosCloud reference client")
    parser.add_argument("OPERATION", nargs="+", help="Operation to perform")
    parser.add_argument(
        "--deployment",
        default="prd",
        choices=("dev", "stg", "prd"),
        help="Deployment to target",
    )
    parser.add_argument(
        "--user",
        default=USER,
        help='User for DEV environments. Defaults to USER from env: ("{}")'.format(
            USER
        ),
    )
    parser.add_argument(
        "--aws-region", default="eu-west-1", help="AWS Region to target"
    )
    parser.add_argument("--client-id", required=True, help="Cognito client-id to use")
    parser.add_argument(
        "--client-secret", required=True, help="Cognito client-id secret"
    )
    parser.add_argument("--apikey", help="APIKEY for interfaces that need it")
    parser.add_argument(
        "--scopes",
        default=None,
        help="Scopes to request. If unset, requests all allowed scopes",
    )
    parser.add_argument(
        "--loglevel",
        default="INFO",
        help="Logger level. Default: INFO",
        choices=("DEBUG", "INFO", "ERROR", "WARNING"),
    )

    args = parser.parse_args()

    logger.setLevel(args.loglevel)
    logger.info("KubiosCloudClient v0.1")

    # Call Cognito for access token
    access_token, error = _get_cognito_access_token(args)
    if error:
        logger.error("Authorization failed: {}".format(error))
        return

    # Construct a requests session which has the required auth headers in place already
    headers = {
        "Authorization": "Bearer {}".format(access_token),
        "X-Api-Key": args.apikey,
    }
    reqs = requests.Session()
    reqs.headers.update(headers)

    # Call a handler identified by the "args.OPERATION" param.
    try:
        result, error = getattr(
            sys.modules[__name__], "do_{}".format(args.OPERATION[0].replace("-", "_"))
        )(args, reqs)
        if error:
            logger.error("Call failed. Got: '{}'".format(error))
        else:
            logger.debug("Call successful. Got:")
            print(json.dumps(result, indent=2))

    except AttributeError:
        logger.error("Invalid operation: {}".format(args.OPERATION[0]))


if __name__ == "__main__":
    main()
