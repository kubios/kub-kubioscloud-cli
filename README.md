KubiosCloud Client
==================

This is a simple reference client for operating against the KubiosCloud APIs.

Prerequisites
-------------

In order to use this client (Or the APIs in general) you need to have proper credentials available. For 3rd party
integration APIs, you will need two sets of credentials:

- OAuth2 client_credentials (--client-id, --client-secret)
- APIKEY (--apikey)

The client_credentials control access and permissions, while the APIKEY controls quota and throttling.

Environment
-----------

In order to run the tool, you must have an environment with "requests" library installed. The repository contains
a Makefile which can build a local virtual environment containing the required libraries. Run:

    make venv
    . ./venv/bin/activate

before trying the actual tool invocations described later.

Operations
----------

### analytics-readiness

This operation calls the "/v2/analytics/analyze" endpoint of the service for
readiness analysis. Format of the parameters:

    <data_format> <internal_value, interval_value, ...> [internal_value, interval_value, ...]

Where the "data_format" can have values "RR" or "PPI", and the "interval_value" is a heartbeat interval
in milliseconds. For example:

    analytics-readiness RR 1000,1100,1110,1100,1099,1100,1200,1200 1000,1100,1110,1100,1099,1100

is a valid request, with format set to "RR" and containing two batches of interval values. Note: the total number
of values must be over 10 for the analysis to work.

### analytics-training

This operation calls the "/v2/analytics/analyze" endpoint of the service for
training analysis. The operation requires data to be provided in json formatted
file. See `training_input_example.json` from Kubioscloud documentation for
example of the input.

Using the client
----------------

Example analytics readiness invocation with client-id, client-secret and apikey set:

    $ python3 kubioscloudcli --client-id 9pfhdeadbeefb6u --client-secret cnnldeadbeefltnl --apikey uhUU3deadbeefY06b \
        analyze-readiness RR 1,1000,1,1000,1,1000,1,1000,1,1000,1,1000
    {
      "analysis": {
        "artefact": 0.0,
        "artefact_level": "GOOD",
        "mean_hr_bpm": 119.88011988011986,
        "mean_rr_ms": 500.50000000000006,
        "pns_index": 1725.3581498120352,
        "readiness": null,
        "rmssd_ms": 58377.83712708554,
        "sd1_ms": 63873.77243196233,
        "sd2_ms": 272021.38003591885,
        "sdnn_ms": 215676.27431328053,
        "sns_index": 2.300331392248133,
        "stress_index": 0.10932290632412857
      },
      "status": "ok"
    }

Example analytics training invocation with client-id, client-secret and apikey set:

    $ python3 kubioscloudcli --client-id 9pfhdeadbeefb6u --client-secret cnnldeadbeefltnl --apikey uhUU3deadbeefY06b \
        analyze-training `training_input_example.json`
