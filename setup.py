"""Setup.py for project."""
from setuptools import setup, find_packages
from git_version import version as git_version


classifiers = [
    # How mature is this project? Common values are
    #   2 - Pre-Alpha
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Developers",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: Implementation :: CPython",
    "License :: Other/Proprietary License",
    "Private :: Do Not Upload",
]

# Project runtime requirements.
install_require = [
    "requests",
    "bson",
    "msgpack"
]

# Tools for running tests etc. but which are not required for software to run in production.
tests_require = [
    "coverage",
    "pytest",
    "flake8",
    "pytest-flake8",
    "pytest-black",
    "pytest-cov",
    "pytest-timeout",
    "setuptools>=12",
    "wheel",
    "requests",
]

with open("README.md", encoding="utf-8") as f:
    readme = f.read()

setup(
    name="kubioscloud-cli",
    description="KubiosCloud reference client",
    license="Kubios",
    url="https://www.kubios.com/",
    long_description=readme,
    version=git_version,
    author="Antti Maula",
    author_email="antti.maula@koodinosturi.fi",
    zip_safe=False,
    packages=find_packages("."),
    classifiers=classifiers,
    install_requires=install_require,
    extras_require={"dev": tests_require},
    setup_requires=("pytest-runner",),
    include_package_data=True,
)
